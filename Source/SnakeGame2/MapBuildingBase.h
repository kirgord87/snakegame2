// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MapBuildingBase.generated.h"

UCLASS()
class SNAKEGAME2_API AMapBuildingBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMapBuildingBase();
	UPROPERTY(EditDefaultsOnly, Category = "Map Specifications (!!!For variables MapSizeX and MapSizeY use a multiple of MapElemSpawnStep!!!)")
		int MapSizeX;
	UPROPERTY(EditDefaultsOnly, Category = "Map Specifications (!!!For variables MapSizeX and MapSizeY use a multiple of MapElemSpawnStep!!!)")
		int MapSizeY;
	UPROPERTY(EditDefaultsOnly, Category = "Map Specifications (!!!For variables MapSizeX and MapSizeY use a multiple of MapElemSpawnStep!!!)")
		int MapElemSpawnStep;
	UPROPERTY(EditDefaultsOnly, Category= "Map Specifications (!!!For variables MapSizeX and MapSizeY use a multiple of MapElemSpawnStep!!!)")
		 FVector StartMapPoint;
	UPROPERTY()
		FVector SnakeStartPoint;
	UPROPERTY(EditDefaultsOnly, Category = "MapActor")
		TSubclassOf<AActor> MapActor;
	UPROPERTY()
		FVector SP;
	UPROPERTY(EditDefaultsOnly, Category = "Spawn Actors")
		TSubclassOf<AActor> FoodActor;
	UPROPERTY()
		FVector FirstFoodSpawnLocation;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void AddMap();

	UFUNCTION()
	void SpawnPlace();

	UFUNCTION()
		FVector GetSP();

	UFUNCTION()
		FVector GetSnakeStartPoint();

	UFUNCTION()
		FVector GetCameraStartPoint();
	
	UFUNCTION()
		void SpawnActor();
	
};
