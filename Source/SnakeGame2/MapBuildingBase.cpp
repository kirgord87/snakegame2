// Fill out your copyright notice in the Description page of Project Settings.


#include "MapBuildingBase.h"

// Sets default values
AMapBuildingBase::AMapBuildingBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StartMapPoint = FVector(0.0f, 0.0f, 0.0f);
	MapSizeX = 1000;
	MapSizeY = 2000;
	MapElemSpawnStep = 100;
	
	
}

// Called when the game starts or when spawned
void AMapBuildingBase::BeginPlay()
{
	Super::BeginPlay();
	AddMap();
	FirstFoodSpawnLocation = FVector(GetSP()); //�� ��������!!
	SpawnActor();
	
}

// Called every frame
void AMapBuildingBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SpawnPlace();
}

void AMapBuildingBase::AddMap()
{
	FVector MapElemSpawnPoint = StartMapPoint;

	for (int i=0; i < MapSizeX; i += MapElemSpawnStep)
	{
		AActor* MapPart = GetWorld()->SpawnActor<AActor>(MapActor,FVector()=MapElemSpawnPoint+FVector(i,0,0), FRotator(0,0,0));
	}
		MapElemSpawnPoint += FVector(0,MapElemSpawnStep,0);

		
		
		for(int i=0; i <= MapSizeY; i += MapElemSpawnStep)
		{
			for (int j = 0; j <= MapSizeX; j += MapElemSpawnStep)
			{
				if (j == 0 || j == MapSizeX-MapElemSpawnStep)
				{
					AActor* MapPart = GetWorld()->SpawnActor<AActor>(MapActor,FVector()= MapElemSpawnPoint+FVector(j,i,0), FRotator(0, 0, 0));
					//MapElemSpawnPoint += FVector(MapElemSpawnStep, 0, 0);
				}
			}
			//MapElemSpawnPoint += FVector(0, MapElemSpawnStep, 0);
		}
		
		MapElemSpawnPoint += FVector(0, MapSizeY, 0);
		
		for (int i = 0; i < MapSizeX; i += MapElemSpawnStep)
		{
			AActor* MapPart = GetWorld()->SpawnActor<AActor>(MapActor, FVector() = MapElemSpawnPoint + FVector(i, 0, 0), FRotator(0, 0, 0));
		}
}

void AMapBuildingBase::SpawnPlace()
{
	int MapMaxX = StartMapPoint.X + MapSizeX;
	int MapMaxY = StartMapPoint.Y + MapSizeY;
	int SPX;
	int SPY;

	do 
	{
		SPX = rand() % MapMaxX;
	} while (SPX <= StartMapPoint.X+MapElemSpawnStep || SPX >= MapMaxX-MapElemSpawnStep || SPX % MapElemSpawnStep > 0);

	do
	{
		SPY = rand() % MapMaxY;
	} while (SPY <= StartMapPoint.Y+MapElemSpawnStep || SPY >= MapMaxY-MapElemSpawnStep || SPY % MapElemSpawnStep > 0);

	FVector NewSpawnPlace(SPX, SPY, StartMapPoint.Z);
	SP = NewSpawnPlace;
	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, FString::Printf(TEXT("Place: %f-%f-%f"), NewSpawnPlace.X, NewSpawnPlace.Y, NewSpawnPlace.Z));
	
}

FVector AMapBuildingBase::GetSP()
{
	return SP;
}

FVector AMapBuildingBase::GetSnakeStartPoint()
{
	int StartX = (StartMapPoint.X + MapSizeX) / 2;
	int StartY = (StartMapPoint.Y + MapSizeY) / 2;
	FVector NewPlace( StartX, StartY, StartMapPoint.Z);
	SnakeStartPoint = NewPlace;

	return SnakeStartPoint;
}

FVector AMapBuildingBase::GetCameraStartPoint()
{
	int StartX = (StartMapPoint.X + MapSizeX) / 2;
	int StartY = (StartMapPoint.Y + MapSizeY) / 2+200;
	int StartZ;
	if (MapSizeX > MapSizeY)
	{
		StartZ = (StartMapPoint.Z + MapSizeX);
	}
	else
		StartZ = (StartMapPoint.Z + MapSizeY)/ 1.4;

	FVector CameraStartPoint(StartX, StartY, StartZ);

	return CameraStartPoint;
}

void AMapBuildingBase::SpawnActor()
{
	int SPX;
	int SPY;
	int MapMaxX = StartMapPoint.X + MapSizeX;
	int MapMaxY = StartMapPoint.Y + MapSizeY;

	do
	{
		SPX = rand() % MapMaxX;
	} while (SPX <= StartMapPoint.X + MapElemSpawnStep || SPX >= MapMaxX - MapElemSpawnStep || SPX % MapElemSpawnStep > 0);

	do
	{
		SPY = rand() % MapMaxY;
	} while (SPY <= StartMapPoint.Y + MapElemSpawnStep || SPY >= MapMaxY - MapElemSpawnStep || SPY % MapElemSpawnStep > 0);

	
	AActor* SpawnActorFood = GetWorld()->SpawnActor<AActor>(FoodActor, FVector(SPX,SPY,StartMapPoint.Z), FRotator());

	//AActor* SpawnActorFood = GetWorld()->SpawnActor<AActor>(FoodActor, FVector(FirstFoodSpawnLocation), FRotator()); !!�� ��������!!
}




