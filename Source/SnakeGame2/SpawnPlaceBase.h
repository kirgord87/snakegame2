// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnPlaceBase.generated.h"

UCLASS()
class SNAKEGAME2_API ASpawnPlaceBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnPlaceBase();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* SpawnPlace;
	
	UPROPERTY()
		float StepSize;

	UPROPERTY()
		TArray<FVector> SpawnPlaces;


	UPROPERTY(BlueprintReadWrite)
		FVector FirstLoc;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		FVector NewRandomLocation;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void AddSpawnPlace();

	UFUNCTION()
		FVector GetRandomLocation();
	
	
};
