// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnPlaceBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASpawnPlaceBase::ASpawnPlaceBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SpawnPlace = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SpawnPlace"));
	StepSize = 100.f;

}

// Called when the game starts or when spawned
void ASpawnPlaceBase::BeginPlay()
{
	Super::BeginPlay();
	//AddSpawnPlace();
}

// Called every frame
void ASpawnPlaceBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//GetRandomLocation();
}



void ASpawnPlaceBase::AddSpawnPlace()
{
	
	FirstLoc = this->GetActorLocation();

	for (int i = 0; i < 10; ++i)
	{
		FVector NewLocX(FirstLoc.X+(i * StepSize), FirstLoc.Y, FirstLoc.Z);
		SpawnPlaces.Add(NewLocX);

		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Blue, FString::Printf(TEXT("Place: %f-%f-%f"), NewLocX.X, NewLocX.Y, NewLocX.Z));
		for (int j = 0; j < 10; j++)
		{
			FVector NewlocY(NewLocX.X, j * StepSize, FirstLoc.Z);
			SpawnPlaces.Add(NewlocY);
			GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, FString::Printf(TEXT("Place: %f-%f-%f"), NewlocY.X, NewlocY.Y, NewlocY.Z));
		}
	}
}

FVector ASpawnPlaceBase::GetRandomLocation()
{
	
		NewRandomLocation = SpawnPlaces[rand() % SpawnPlaces.Num()];
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Yellow, FString::Printf(TEXT("Place: %f-%f-%f"), NewRandomLocation.X, NewRandomLocation.Y, NewRandomLocation.Z));



		return NewRandomLocation;
 
}




