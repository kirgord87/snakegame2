// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "EngineUtils.h"
#include "MapBuildingBase.h"


// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//SetActorLocation(GetLocation());

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	//SetActorLocation(GetLocation());
}
	
	


// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	
}

void AFood::Interact(AActor* Interactor,bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
		}
		GetLocation();
		Destroy();
		SpawnObject(NewLocation);
	}
}

void AFood::SpawnObject(FVector Loc)
{
	AActor* SpawnedActorRef = GetWorld()->SpawnActor<AActor>(ActorToSpawn,Loc, FRotator());
}

FVector AFood::GetLocation()
{
	AMapBuildingBase* MyMap;
	for (TActorIterator<AMapBuildingBase>ActorIt(GetWorld()); ActorIt; ++ActorIt)
	{
		MyMap = *ActorIt;
		NewLocation = MyMap->GetSP();
		break;
	}
	
	


	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Blue, FString::Printf(TEXT("Place: %f-%f-%f"), NewLocation.X, NewLocation.Y, NewLocation.Y));
	return NewLocation;
}

